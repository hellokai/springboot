package com.lck;

import com.lck.common.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 项目测试类
 *
 * @author Hellokai
 * @date 2023/03/14 15:30
 */
@Slf4j
@SpringBootTest(classes = SpringbootApplication.class)
public class SpringbootTest {


    @Test
    public void testDate(){

        log.debug("get year : {}",DateUtils.getYear());
        log.debug("get month : {}",DateUtils.getMonth());
        log.debug("get today : {}",DateUtils.getToday());

    }


}
