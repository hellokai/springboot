package com.lck.controller;

import com.lck.common.enums.ResponseEnum;
import com.lck.common.web.BaseController;
import com.lck.common.web.CommonResponse;
import com.lck.entity.Demo;
import com.lck.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 演示案例 控制层
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
@RestController
@RequestMapping("/demo")
public class DemoController extends BaseController {

    @Autowired
    private DemoService demoService;

    /**
     * 根据ID查
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    public CommonResponse getDemoById( @RequestParam(value = "id",required = true) Long id) {
        if(id <= 0){
            logger.info("query Demo param error,id = {}",id);
            return CommonResponse.error(ResponseEnum.PARAM_ERROR);
        }
        Demo demo = demoService.getDemoById(id);
        return CommonResponse.success(demo);
    }








}
