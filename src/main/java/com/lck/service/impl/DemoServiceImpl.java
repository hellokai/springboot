package com.lck.service.impl;

import com.lck.entity.Demo;
import com.lck.mapper.DemoMapper;
import com.lck.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 演示案例 服务层实现
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
@Slf4j
@Service("demoService")
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoMapper demoMapper;


    /**
     * 根据 id 查询
     * 获取策略：先从缓存中获取，没有则从数据表中获取，再将数据写入缓存。
     *
     * @param id
     * @return
     */
    @Override
    public Demo getDemoById(Long id) {
        try{
            Demo demo = demoMapper.getDemoById(id);
            log.info("query Demo success,id = {}",id);
            return demo;
        }catch (Exception e){
            log.error("query Demo error",e);
        }
        return null;
    }


}
