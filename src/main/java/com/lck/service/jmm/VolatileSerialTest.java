package com.lck.service.jmm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * volatile 有序性测试
 *
 * @author Create by hellokai
 * @date 2020/5/10 16:02
 **/
public class VolatileSerialTest {

    public static volatile int x = 0, y = 0;

    public static void main(String[] args) throws InterruptedException {
        Set<String> resultSet = new HashSet<>();
        Map<String,Integer> resultMap = new HashMap<>();
        for(int i = 0; i < 10000; i++){
            x = 0; y = 0;
            resultMap.clear();
            Thread threadA = new Thread(new Runnable() {
                @Override
                public void run() {
                    int a = y;
                    x = 1;
                    resultMap.put("a",a);
                }
            });

            Thread threadB = new Thread(new Runnable() {
                @Override
                public void run() {
                    int b = x;
                    y = 1;
                    resultMap.put("b",b);
                }
            });

            threadA.start();
            threadB.start();
            threadA.join();
            threadB.join();

            resultSet.add("a=" + resultMap.get("a") + "," + "b=" + resultMap.get("b"));
            System.out.println(resultSet);
        }

    }
    /**
     * 1.不用volatile如何保证重排序 如何手动添加内存屏障？
     * Unsafe
     *
     * 2.总线风暴问题?
     * 线程交互太多，占用总线通讯，嗅探等，导致无效缓存过多，总线阻塞，解决办法就是适当得用synchronized 减少总线繁忙
     *
     */


}
