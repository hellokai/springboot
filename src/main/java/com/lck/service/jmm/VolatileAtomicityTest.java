package com.lck.service.jmm;

/**
 * volatile原子性测试
 *
 * @author Create by hellokai
 * @date 2020/5/10 16:02
 **/
public class VolatileAtomicityTest {

    // volatile
    private static volatile int num = 0;

    public static void add(){
        num ++;
    }

    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[10];
        for(int i = 0; i < threads.length; i++){
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for(int i = 0; i < 1000; i++){
                        add();
                    }
                }
            });
            threads[i].start();
        }

        for (Thread t : threads) {
            t.join();
        }

        // 10 * 1000 结果值是多少?
        System.out.println(num);
    }

    /**
     * 1.不用volatile如何保证一致性?
     *
     */


}
