package com.lck.service.jmm;

/**
 * volatile可见性测试
 *
 * @author Create by hellokai
 * @date 2020/5/10 15:20
 **/
public class VolatileVisibilityTest {

    // 是否volatile
    private static  boolean initFlag = false;

    public void save(){
        this.initFlag = true;
        System.out.println("线程：" + Thread.currentThread().getName() + ":修改共享变量initFlag");
    }

    public void load(){
        System.out.println("线程：" + Thread.currentThread().getName() + "当前线程正在执行");
        while (!initFlag){
        //线程在此处空跑，等待initFlag状态改变
        }
        System.out.println("线程：" + Thread.currentThread().getName() + "当前线程嗅探到initFlag的状态的改变");
    }


    public static void main(String[] args){

        VolatileVisibilityTest test = new VolatileVisibilityTest();
        Thread threadA = new Thread(()->{
            test.load();
        },"threadA");

        Thread threadB = new Thread(()->{
            test.save();
        },"threadB");

        threadA.start();

        try {
            // 睡1秒
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        threadB.start();
    }

}
