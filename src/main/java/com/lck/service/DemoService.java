package com.lck.service;

import com.lck.entity.Demo;


/**
 * 演示案例 服务层
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
public interface DemoService {

    /**
     * 根据ID查
     *
     * @param id
     * @return
     */
    Demo getDemoById(Long id);



}
