package com.lck.entity;


import lombok.Data;

/**
 * 信息表 db_demo
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
@Data
public class Demo {

    /**主键*/
    private Integer id;

    /**demo名称*/
    private String demoName;

    /**demo信息*/
    private String demoInfo;

}
