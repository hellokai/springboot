package com.lck.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 演示出参
 * @author Create by hellokai
 * @date 2022/03/25 18:30
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DemoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**主键*/
    private Long id;
    /**demo名称*/
    private String demoName;
    /**demo信息*/
    private String demoInfo;

}
