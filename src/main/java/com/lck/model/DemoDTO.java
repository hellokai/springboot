package com.lck.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 演示入参
 *
 * @author Create by hellokai
 * @date 2022/03/25 18:30
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = false)
public class DemoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**主键*/
    private Long id;
    /**demo名称*/
    private String demoName;
    /**demo信息*/
    private String demoInfo;

}
