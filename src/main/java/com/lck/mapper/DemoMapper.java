package com.lck.mapper;

import com.lck.entity.Demo;
import com.lck.model.DemoDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 演示案例 数据层
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
@Mapper
public interface DemoMapper {

    /**
     * 根据条件分页查询Demo数据
     *
     * @param demoDTO demo信息
     * @return demo数据集合
     */
    List<Demo> queryDemo(DemoDTO demoDTO);

    /**
     * 根据 id 查询
     *
     * @param id demoID
     * @return
     */
    Demo getDemoById(@Param("id") long id);

    /**
     * 根据 demo 查询
     *
     * @param demo
     * @return
     */
    int updateDemo(@Param("demo") Demo demo);

    /**
     * 根据 id 删除
     *
     * @param id
     * @return
     */
    int deleteDemoById(long id);

}
