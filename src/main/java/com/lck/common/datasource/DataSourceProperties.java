package com.lck.common.datasource;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 数据源参数
 *
 * @author Create by hellokai
 * @date 2023/11/29 16:58
 **/
@Data
@Component
@ConfigurationProperties(prefix = "db.druid")
public class DataSourceProperties {

    private int initialSize;

    private int minIdle;

    private int maxActive;

    private int maxWait;


    private int connectTimeout;

    private int socketTimeout;


    private int timeBetweenEvictionRunsMillis;

    private int minEvictableIdleTimeMillis;

    private int maxEvictableIdleTimeMillis;


    private String validationQuery;

    private boolean testWhileIdle;

    private boolean testOnBorrow;

    private boolean testOnReturn;


    private boolean poolPreparedStatements;

    private int maxOpenPreparedStatements;

    private boolean keepAlive;

}
