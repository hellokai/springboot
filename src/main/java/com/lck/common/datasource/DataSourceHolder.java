package com.lck.common.datasource;

/**
 * 数据源持有者
 *
 * @author Create by hellokai
 * @date 2023/11/28 15:25
 **/
public class DataSourceHolder {

    /**
     * 线程安全数据源上下文持有者
     */
    private static final ThreadLocal<String> DATA_SOURCE_HOLDER = new ThreadLocal<>();

    /**
     * 设置数据源
     *
     * @param dataSourceName 数据源名称
     */
    public static void set(String dataSourceName) {
        DATA_SOURCE_HOLDER.set(dataSourceName);
    }

    /**
     * 获取数据源
     */
    public static String get() {
        return DATA_SOURCE_HOLDER.get();
    }

    /**
     * 清除数据源
     */
    public static void remove() {
        DATA_SOURCE_HOLDER.remove();
    }

}
