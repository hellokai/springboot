package com.lck.common.datasource;

import java.lang.annotation.*;

/**
 * 数据源注解
 *
 * @author Create by hellokai
 * @date 2023/11/28 15:48
 **/
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSourceType {

    /**
     * 数据源，默认主数据源
     *
     * @return 数据源名称
     */
    DataSourceEnum value() default DataSourceEnum.MASTER;

}
