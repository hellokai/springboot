package com.lck.common.datasource;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * 数据源AOP
 *
 * @author Create by Administrator
 * @date 2023/11/28 15:41
 **/
@Slf4j
@Aspect
@Component
public class DataSourceAspect {

    @Pointcut("@annotation(com.lck.common.datasource.DataSourceType)")
    private void dataSourceMethodPointCut() {
    }

    @Pointcut("execution(public * com.lck..*(..))" +
            "&& @target(com.lck.common.datasource.DataSourceType)")
    private void dataSourceClassPointCut() {
    }

    @Around("dataSourceMethodPointCut()")
    public Object doAroundMethod(ProceedingJoinPoint point) throws Throwable{

        MethodSignature methodSignature = (MethodSignature) point.getSignature();
        DataSourceType dataSourceType = methodSignature.getMethod().getAnnotation(DataSourceType.class);

        String dataSource = dataSourceType.value().name();
        DataSourceHolder.set(dataSource);
        log.info("method set data source is " + dataSource);

        try {
            return point.proceed();
        } finally {
            DataSourceHolder.remove();
            log.info("method remove data source is " + dataSource);
        }

    }

    @Around("dataSourceClassPointCut()")
    public Object doAroundClass(ProceedingJoinPoint point) throws Throwable{

        Class<?> clazz = point.getTarget().getClass();
        DataSourceType dataSourceType = clazz.getAnnotation(DataSourceType.class);

        String dataSource = dataSourceType.value().name();
        DataSourceHolder.set(dataSource);
        log.info("class set data source is " + dataSource);

        try {
            return point.proceed();
        } finally {
            DataSourceHolder.remove();
            log.info("class remove data source is " + dataSource);
        }

    }


}
