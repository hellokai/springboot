package com.lck.common.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据源配置
 *
 * @author Create by hellokai
 * @date 2023/11/27 17:10
 **/
@Slf4j
@Configuration
public class DataSourceConfig {

    @Autowired
    private DataSourceProperties dataSourceProperties;


    /**
     * master数据源
     *
     * @return 数据源
     */
    @Bean("masterDataSource")
    @ConfigurationProperties(prefix = "db.master")
    public DataSource masterDataSource(){
        DruidDataSource druidDataSource = DruidDataSourceBuilder.create().build();
        return getDruidDataSource(druidDataSource);
    }

    /**
     * second数据源
     *
     * @return 数据源
     */
    @Bean("masterDataSource")
    @ConfigurationProperties(prefix = "db.second")
    public DataSource secondDataSource(){
        DruidDataSource druidDataSource = DruidDataSourceBuilder.create().build();
        return getDruidDataSource(druidDataSource);
    }

    /**
     * 动态数据源
     *
     * @return 数据源
     */
    @Primary
    @Bean(name = "dynamicDataSource")
    public DataSource dynamicDataSource() {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        // 默认数据源
        dynamicDataSource.setDefaultTargetDataSource( masterDataSource() );

        // 配置多数据源
        Map<Object, Object> dsMap = new HashMap<>();
        dsMap.put(DataSourceEnum.MASTER.name(), masterDataSource());
        dsMap.put(DataSourceEnum.SECOND.name(), secondDataSource());

        dynamicDataSource.setTargetDataSources(dsMap);
        return dynamicDataSource;
    }

    /**
     * 事务管理器
     *
     * @return 事务管理器
     */
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dynamicDataSource) {
        return new DataSourceTransactionManager( dynamicDataSource );
    }


    /**
     * 数据源参数
     *
     * @param datasource 数据源
     * @return 数据源
     */
    private DruidDataSource getDruidDataSource(DruidDataSource datasource){
        //配置初始化大小、最小、最大
        datasource.setInitialSize(dataSourceProperties.getInitialSize());
        datasource.setMinIdle(dataSourceProperties.getMinIdle());
        datasource.setMaxActive(dataSourceProperties.getMaxActive());

        //配置获取连接等待超时的时间
        datasource.setMaxWait(dataSourceProperties.getMaxWait());

        // 配置驱动连接超时时间，检测数据库建立连接的超时时间，单位是毫秒
        datasource.setConnectTimeout(dataSourceProperties.getConnectTimeout());

        // 配置网络超时时间，等待数据库操作完成的网络超时时间，单位是毫秒
        datasource.setSocketTimeout(dataSourceProperties.getSocketTimeout());

        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        datasource.setTimeBetweenEvictionRunsMillis(dataSourceProperties.getTimeBetweenEvictionRunsMillis());

        // 配置一个连接在池中最小、最大生存的时间，单位是毫秒
        datasource.setMinEvictableIdleTimeMillis(dataSourceProperties.getMinEvictableIdleTimeMillis());
        datasource.setMaxEvictableIdleTimeMillis(dataSourceProperties.getMaxEvictableIdleTimeMillis());

        // 用来检测连接是否有效的sql，要求是一个查询语句，常用select 'x'。
        // 如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会起作用。
        datasource.setValidationQuery(dataSourceProperties.getValidationQuery());
        // 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，
        // 如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
        datasource.setTestWhileIdle(dataSourceProperties.isTestWhileIdle());
        // 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
        datasource.setTestOnBorrow(dataSourceProperties.isTestOnBorrow());
        // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
        datasource.setTestOnReturn(dataSourceProperties.isTestOnReturn());

        datasource.setPoolPreparedStatements(dataSourceProperties.isPoolPreparedStatements());
        datasource.setMaxOpenPreparedStatements(dataSourceProperties.getMaxOpenPreparedStatements());
        datasource.setKeepAlive(dataSourceProperties.isKeepAlive());

        log.info("datasource hashCode: {}",datasource.hashCode());
        return datasource;
    }


}
