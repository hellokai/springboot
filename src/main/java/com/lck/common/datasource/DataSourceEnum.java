package com.lck.common.datasource;

/**
 * 数据源枚举
 *
 * @author Create by Administrator
 * @date 2023/11/28 18:57
 **/
public enum DataSourceEnum {

    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SECOND;

}
