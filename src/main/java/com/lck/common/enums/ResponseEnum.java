package com.lck.common.enums;

public enum ResponseEnum {

    /**请求成功*/
    SUCCESS("0000","success"),

    /**请求失败*/
    FAIL("9999","fail"),

    /**查询失败*/
    QUERY_ERROR("1000","查询失败！"),

    /**保存失败*/
    SAVE_ERROR("2000","保存失败！"),

    /**编辑失败*/
    EDIT_ERROR("3000","编辑失败！"),

    /**删除失败*/
    DELETE_ERROR("4000","删除失败！"),

    /**参数错误*/
    PARAM_ERROR("5000","参数错误！");

    private String code;
    private String message;

    ResponseEnum(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }
    public String getMessage(){
        return  message;
    }
}
