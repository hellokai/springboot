package com.lck.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具类
 *
 * @author Hellokai
 * @date 2022/03/25 18:30
 */
public class DateUtils {

    public static final String  DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DATETIME_ZH_FORMAT = "yyyy年MM月dd日HH时mm分ss秒";
    public static final String DATE_ZH_FORMAT = "yyyy年MM月dd日";
    public static final String TIME_ZH_FORMAT = "HH时mm分ss秒";

    public static final String TOSEC = "yyyyMMddHHmmss";
    public static final String TODAY = "yyyyMMdd";
    public static final String MONTH = "yyyyMM";
    public static final String YEAR = "yyyy";

    private static final ThreadLocal<SimpleDateFormat> SIMPLE_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<>();


    /**
     * 实例化SimpleDateFormat对象
     */
    private static SimpleDateFormat getFormat(String formatType){
        switch (formatType){
            case DATETIME_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(DATETIME_FORMAT));
                break;

            case DATE_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(DATE_FORMAT));
                break;

            case TIME_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(TIME_FORMAT));
                break;

            case DATETIME_ZH_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(DATETIME_ZH_FORMAT));
                break;

            case DATE_ZH_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(DATE_ZH_FORMAT));
                break;

            case TIME_ZH_FORMAT:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(TIME_ZH_FORMAT));
                break;

            case TOSEC:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(TOSEC));
                break;

            case TODAY:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(TODAY));
                break;

            case MONTH:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(MONTH));
                break;

            case YEAR:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(YEAR));
                break;

            default:
                SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(formatType));
                break;
        }
        return SIMPLE_DATE_FORMAT_THREAD_LOCAL.get();
    }

    /**
     * 格式化时间,默认格式 yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return strTime
     */
    public static String dateFormat(Date date) {
        String strTime = "";
        try {
            strTime = getFormat(DATETIME_FORMAT).format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strTime;
    }

    /**
     * 根据formatType格式化时间
     *
     * @param date
     * @param formatType
     * @return strTime
     */
    public static String dateFormatByType(Date date, String formatType) {
        String strTime = "";
        try {
            strTime = getFormat(formatType).format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strTime;
    }

    /**
     * 解析时间字符串,默认
     *
     * @param dateStr
     * @return date
     */
    public static Date dateParse(String dateStr) {
        Date date = null;
        try {
            date = getFormat(DATETIME_FORMAT).parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 根据formatType解析时间字符串
     *
     * @param dateStr
     * @param formatType
     * @return date
     */
    public static Date dateParseByType(String dateStr, String formatType) {
        Date date = null;
        try {
            date = getFormat(formatType).parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 获取当月第一天
     *
     * @return date
     */
    public static String getFirstDayOfMonth() {
        Calendar cal = Calendar.getInstance();
        // 年
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        // 月，因为Calendar里的月是从0开始，所以要减1
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        // 日，设为一号
        cal.set(Calendar.DATE, 1);
        // 获得月初是几号
        return getFormat(DATE_FORMAT).format(cal.getTime());
    }

    /**
     * 获取当月最后一天
     *
     * @return date
     */
    public static String getLastDayOfMonth() {
        Calendar cal = Calendar.getInstance();
        // 年
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        // 月，因为Calendar里的月是从0开始，所以要减1
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        // 日，设为一号
        cal.set(Calendar.DATE, 1);
        // 月份加一，得到下个月的一号
        cal.add(Calendar.MONTH, 1);
        // 下一个月减一为本月最后一天
        cal.add(Calendar.DATE, -1);
        // 获得月末是几号
        return getFormat(DATE_FORMAT).format(cal.getTime());
    }

    /**
     * 获取指定月第一天
     *
     * @param year
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR,year);
        // 设置月份
        cal.set(Calendar.MONTH, month-1);
        // 获取某月最小天数
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        // 获得月初是几号
        return getFormat(DATE_FORMAT).format(cal.getTime());
    }

    /**
     * 获取指定月最后一天
     *
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR,year);
        // 设置月份
        cal.set(Calendar.MONTH, month-1);
        // 获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        // 获得月末是几号
        return getFormat(DATE_FORMAT).format(cal.getTime());
    }

    /**
     * 获取当前日期N天后的日期,参数为负N天前
     *
     * @param days
     * @return
     */
    public static Date getAddDaysAfter(int days) {
        Calendar cal = Calendar.getInstance();
        // 设置日历中增加天数
        cal.add(Calendar.DAY_OF_MONTH, days);
        // 获得日期
        return cal.getTime();
    }

    /**
     * 获取指定日期N天后的日期,参数为负N天前
     *
     * @param date
     * @param days
     * @return
     */
    public static Date getAddDaysAfter(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        // 设置指定时间
        cal.setTime(date);
        // 设置日历中增加天数
        cal.add(Calendar.DAY_OF_MONTH, days);
        // 获得日期
        return cal.getTime();
    }

    /**
     * 根据区间开始日期,将yyyy-MM-dd 解析为 yyyy-MM-dd 00:00:00
     *
     * @param dateStr yyyy-MM-dd格式
     * @return date
     */
    public static Date parseStartDate(String dateStr){
        Date date = null;
        try{
            date = getFormat(DATETIME_FORMAT).parse(dateStr+" 00:00:00");
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 根据区间结束日期,将yyyy-MM-dd 解析为 yyyy-MM-dd 23:59:59
     *
     * @param dateStr yyyy-MM-dd格式
     * @return date
     */
    public static Date parseEndDate(String dateStr){
        Date date = null;
        try{
            date = getFormat(DATETIME_FORMAT).parse(dateStr+" 23:59:59");
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 根据日期,判断所属月第几周
     *
     * @param dateStr yyyy-MM-dd格式
     * @return
     * @throws Exception
     */
    public static int getWeek(String dateStr){
        Calendar calendar = Calendar.getInstance();
        int week = 0;
        try{
            Date date = getFormat(DATE_FORMAT).parse(dateStr);
            calendar.setTime(date);
            //第几周
            week = calendar.get(Calendar.WEEK_OF_MONTH);
            //第几天，从周日开始
            //int day = calendar.get(Calendar.DAY_OF_WEEK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return week;
    }

    /**
     * 根据日期,判断是周几
     *
     * @param dateStr yyyy-MM-dd格式
     * @return
     */
    public static String getWeekDay(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        Date d = null;
        try {
            d = getFormat(DATE_FORMAT).parse(dateStr);
        } catch (ParseException e) {
            e.getMessage();
        }
        return sdf.format(d);
    }

    /**
     * 记录考勤, 记录迟到, 早退时间
     *
     * @return
     */
    public static String getState() {
        String state = "正常";

        Date d = new Date();
        try {
            Date d1 = getFormat(TIME_FORMAT).parse("08:00:00");
            Date d2 = getFormat(TIME_FORMAT).parse(getFormat(TIME_FORMAT).format(d));
            Date d3 = getFormat(TIME_FORMAT).parse("17:30:00");

            int t1 = (int) d1.getTime();
            int t2 = (int) d2.getTime();
            int t3 = (int) d3.getTime();
            if (t2 < t1) {
                // 除以1000是为了转换成秒
                long between = (t1 - t2) / 1000;
                long hour1 = between % (24 * 3600) / 3600;
                long minute1 = between % 3600 / 60;

                state = "迟到 ：" + hour1 + "时" + minute1 + "分";

            } else if (t2 < t3) {
                // 除以1000是为了转换成秒
                long between = (t3 - t2) / 1000;
                long hour1 = between % (24 * 3600) / 3600;
                long minute1 = between % 3600 / 60;
                state = "早退 ：" + hour1 + "时" + minute1 + "分";
            }
            return state;
        } catch (Exception e) {
            return state;
        }
    }

    /**
     * 获取年
     *
     * @return
     */
    public static String getYear(){
        return LocalDate.now().format(DateTimeFormatter.ofPattern(YEAR));
    }

    /**
     * 获取月
     *
     * @return
     */
    public static String getMonth(){
        return LocalDate.now().format(DateTimeFormatter.ofPattern(MONTH));
    }

    /**
     * 获取日
     *
     * @return
     */
    public static String getToday(){
        return LocalDate.now().format(DateTimeFormatter.ofPattern(TODAY));
    }


}
