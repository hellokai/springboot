package com.lck.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置类
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
@Configuration
public class ThreadPoolConfig {

    @Autowired
    private Environment env;

    @Bean("getDefaultExecutor")
    public ThreadPoolTaskExecutor getDefaultExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize( Integer.parseInt( env.getProperty( "thread.corePoolSize" ) ) );
        // 设置最大线程数
        executor.setMaxPoolSize( Integer.parseInt( env.getProperty( "thread.maxPoolSize" ) ) );
        // 设置队列容量
        executor.setQueueCapacity( Integer.parseInt( env.getProperty( "thread.queueCapacity" ) ) );
        // 设置允许的空闲时间（秒）
        executor.setKeepAliveSeconds( Integer.parseInt( env.getProperty( "thread.keepAliveSeconds" ) ) );
        // 设置默认线程名称 ,不设置则默认方法名
        executor.setThreadNamePrefix("Example-Default-Thread-");
        // 设置拒绝策略rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }

}
