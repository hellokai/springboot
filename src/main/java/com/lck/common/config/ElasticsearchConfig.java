package com.lck.common.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * ES 配置
 *
 * @author Create by hellokai
 * @date 2023/11/21 14:04
 **/
@Configuration
public class ElasticsearchConfig {

    /**连接超时时间,默认1000*/
    private final int connectTimeOut = 5000;
    /**连接超时时间,默认30000*/
    private final int socketTimeOut = 30000;
    /**获取连接的超时时间,默认*/
    private final int connectionRequestTimeOut = 500;
    /**最大连接数,默认30*/
    private final int maxConnectNum = 100;
    /**最大路由连接数,默认10*/
    private final int maxConnectPerRoute = 100;
    /**权限验证*/
    final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

    @Value("${db.elasticsearch.username}")
    private String USERNAME;
    @Value("${db.elasticsearch.password}")
    private String PASSWORD;
    @Value("${db.elasticsearch.uris}")
    private String URLS;

    @Bean(name = "elasticsearchClient")
    public ElasticsearchClient elasticsearchClient(){

        // 1.配置ES安全认证(默认用户名为elastic)
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(USERNAME, PASSWORD));

        RestClient client = RestClient.builder( getHttpHost() )
                .setRequestConfigCallback(
                        requestConfigBuilder ->
                                requestConfigBuilder
                                        .setConnectTimeout(connectTimeOut)
                                        .setSocketTimeout(socketTimeOut)
                                        .setConnectionRequestTimeout(connectionRequestTimeOut))

                .setHttpClientConfigCallback(
                        httpClientBuilder ->
                                httpClientBuilder
                                        .setDefaultCredentialsProvider(credentialsProvider)
                                        .setMaxConnTotal(maxConnectNum)
                                        .setMaxConnPerRoute(maxConnectPerRoute))
                .build();

        ElasticsearchTransport transport = new RestClientTransport(client,new JacksonJsonpMapper());
        return new ElasticsearchClient(transport);
    }


    /**
     * 处理请求地址
     *
     * @return HttpHost
     */
    private HttpHost[] getHttpHost() {

        if (!StringUtils.hasLength(URLS)) {
            throw new RuntimeException("spring.elasticsearch.rest.urls is null");
        }
        // 1.做好分割
        String[] urls = URLS.split(",");
        HttpHost[] httpHostArr = new HttpHost[urls.length];
        for (int i=0; i<urls.length; i++) {
            String urlStr = urls[i];
            if(!StringUtils.hasLength(urlStr)) {
                continue;
            }
            httpHostArr[i] = HttpHost.create(urlStr);
        }
        return httpHostArr;
    }


}
