package com.lck.common.config.redis;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.filter.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.util.Objects;

/**
 * FastJson2实现Redis序列化
 *
 * @author Create by hellokai
 * @date 2023/11/17 16:36
 **/
@Slf4j
public class FastJson2JsonRedisSerializer <T> implements RedisSerializer<T> {

    public static final Filter AUTO_TYPE_FILTER = JSONReader.autoTypeFilter(
            // 需要支持自动类型的类名前缀(或其他)，范围越小越安全
            "com.***.***","com.***.***"
    );

    private Class<T> clazz;

    public FastJson2JsonRedisSerializer(Class<T> clazz) {
        super();
        this.clazz = clazz;
    }

    @Override
    public byte[] serialize(T t) throws SerializationException {
        if(Objects.isNull(t)){
            return new byte[0];
        }
        try{
            return JSON.toJSONBytes(t, JSONWriter.Feature.WriteClassName);
        }catch (Exception e){
            log.error("FastJson2 redis serialize error: {}",e.getMessage());
            throw new SerializationException(" not serialize: " + e.getMessage(), e);
        }
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        if(bytes == null || bytes.length <= 0){
            return null;
        }
        try{
            return JSON.parseObject(bytes,clazz,AUTO_TYPE_FILTER);
        }catch (Exception e){
            log.error("FastJson2 redis unserialize error: {}",e.getMessage());
            throw new SerializationException(" not deserialize: " + e.getMessage(), e);
        }
    }


}
