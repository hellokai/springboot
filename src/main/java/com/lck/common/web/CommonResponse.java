package com.lck.common.web;

import com.lck.common.enums.ResponseEnum;
import com.lck.common.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 响应信息
 *
 * @author hellokai
 * @date 2023/7/7 14:12
 **/
public class CommonResponse<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String message;
    private String timestamp;
    private T data;


    public CommonResponse(){}


    public CommonResponse(T data, ResponseEnum responseEnum){
        this.code = responseEnum.getCode();
        this.message = responseEnum.getMessage();
        this.timestamp = DateUtils.dateFormatByType(new Date(), DateUtils.TOSEC);
        this.data = data;
    }


    public static <T> CommonResponse<T> success(){
        return new CommonResponse("", ResponseEnum.SUCCESS);
    }

    public static <T> CommonResponse<T> success(T data){
        return new CommonResponse(data, ResponseEnum.SUCCESS);
    }

    public static <T> CommonResponse<T> error( ResponseEnum responseEnum){
        return new CommonResponse(null, responseEnum);
    }

    public static <T> CommonResponse<T> error( String code, String message){
        return new CommonResponse<T>()
                .setCode(code)
                .setMessage(message == null ? "" : message)
                .setTimestamp(DateUtils.dateFormatByType(new Date(), DateUtils.TOSEC))
                .setData(null);
    }


    public String getCode() {
        return code;
    }

    public CommonResponse setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public CommonResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public CommonResponse setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public T getData() {
        return data;
    }

    public CommonResponse setData(T data) {
        this.data = data;
        return this;
    }


}
