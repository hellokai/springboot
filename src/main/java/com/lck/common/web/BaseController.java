package com.lck.common.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通用数据处理
 *
 * @author hellokai
 * @date 2022/03/25 18:30
 **/
public class BaseController {

    protected final Logger logger = LoggerFactory.getLogger( this.getClass() );


}
